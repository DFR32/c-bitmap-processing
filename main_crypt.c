#include "main_crypt.h"

int main_crypt(int argc, char** argv)
{
    bmp_image bmp;

    char* file_path = (char*)calloc(MAX_PATH + 1, sizeof(char));
    char* key_path  = (char*)calloc(MAX_PATH + 1, sizeof(char));
    char* dst_path  = (char*)calloc(MAX_PATH + 1, sizeof(char));

    assert( (file_path != NULL) );
    assert( (file_path != NULL) );

    chi_array chi_test = (chi_array)calloc(3, sizeof(double));

    assert( (file_path != NULL) );

    char mode = 0;

    printf("Please enter the operation mode (e - encryption, d - decryption).\n");

    mode = getchar();

    if((mode != 'e') && (mode != 'd'))
    {
        printf("Invalid mode! Closing.\n");
        goto EXIT_PATH;
    }

    printf("Enter the file path:\n");
    scanf("%128s", file_path);

    printf("Please enter the key file name:\n");
    scanf("%128s", key_path);

    printf("Enter the destination path:\n");
    scanf("%128s", dst_path);

    if(mode == 'e')
    {
        read_image(&bmp, file_path);

        chi_result(&bmp, chi_test);
        printf("Chi test before encryption:\n[%.2f %.2f %.2f]\n", chi_test[0], chi_test[1], chi_test[2]);

        encrypt_image(key_path, &bmp);

        reset_chi_result(chi_test);
        chi_result(&bmp, chi_test);
        printf("Chi test after encryption:\n[%.2f %.2f %.2f]\n", chi_test[0], chi_test[1], chi_test[2]);

        write_image(&bmp, dst_path);
        printf("Encryption was successful.\n");
    }
    else if(mode == 'd')
    {
        read_image(&bmp, file_path);

        chi_result(&bmp, chi_test);
        printf("Chi test before decryption:\n[%.2f %.2f %.2f]\n", chi_test[0], chi_test[1], chi_test[2]);

        decrypt_image(key_path, &bmp);

        reset_chi_result(chi_test);
        chi_result(&bmp, chi_test);
        printf("Chi test after decryption:\n[%.2f %.2f %.2f]\n", chi_test[0], chi_test[1], chi_test[2]);

        write_image(&bmp, dst_path);

        printf("Decryption was successful.\n");
    }

    EXIT_PATH:
    free(chi_test);
    free(file_path);
    free(key_path);
    free(dst_path);
    return 0;
}
