#include "main_crypt.h"
#include "main_image_processing.h"

int main(int argc, char** argv)
{
    main_crypt(argc, argv);
    main_image_processing(argc, argv);
    return 0;
}
