#include "pattern_helper.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

uint8_t at(pixel_vector v, uint32_t i, uint32_t j, uint32_t width)
{
    return v[i * width + j][0];
}

void to_grayscale_image(bmp_image* bmp, bmp_image* return_bmp)
{
    uint32_t image_size_bytes = sizeof(rgb_pixel) * bmp->image_height * bmp->image_width;
    uint32_t image_size = bmp->image_height * bmp->image_width;

    uint8_t grayscale_pixel;

    memcpy(return_bmp->header, bmp->header, sizeof(bmp_header));
    return_bmp->image_height = bmp->image_height;
    return_bmp->image_width = bmp->image_width;
    return_bmp->padding = bmp->padding;

    return_bmp->image = (pixel_vector)malloc(image_size_bytes);

    assert( (return_bmp->image != NULL) );

    for(uint32_t i = 0; i < image_size; ++i)
    {
            grayscale_pixel = (uint8_t)((double)bmp->image[i][0] * 0.114 + (double)bmp->image[i][1] * 0.587 + (double)bmp->image[i][2] * 0.299);

            return_bmp->image[i][0] = grayscale_pixel;
            return_bmp->image[i][1] = grayscale_pixel;
            return_bmp->image[i][2] = grayscale_pixel;
    }
}

uint8_t calculate_grayscale_mean_intensity(bmp_image* bmp)
{
    uint32_t image_size = bmp->image_height * bmp->image_width;
    uint64_t sum = 0;

    for(uint32_t i = 0; i < image_size; ++i)
    {
        sum += bmp->image[i][0];
    }

    return (uint8_t)((sum / image_size) & 0xFF);
}


void load_samples(const char* cst_path, const char* extension, digit_array* sample_array, uint32_t sample_count)
{
    char path[MAX_PATH + 1] = {};
    bmp_image bmp, grayscale_img;
    uint32_t width, height, image_size, i = 0;
    uint64_t pixel_sum = 0;

    double tmp_mean, tmp_deviation = 0;

    *sample_array = (digit_array)malloc(sizeof(grayscale_image) * sample_count);

    assert( (sample_array != NULL) );

    for(uint32_t smp_ctr = 0; smp_ctr < sample_count; ++smp_ctr)
    {
        tmp_mean = 0.0f;
        tmp_deviation = 0.0f;
        pixel_sum = 0;

        memset(path, 0, MAX_PATH);
        snprintf(path, MAX_PATH, "%s%u.%s", cst_path, smp_ctr, extension);

        read_image(&bmp, path);

        to_grayscale_image(&bmp, &grayscale_img);

        width = grayscale_img.image_width;
        height = grayscale_img.image_height;
        image_size = width * height;

        for(i = 0; i < image_size; ++i)
        {
            pixel_sum += grayscale_img.image[i][0];
        }

        tmp_mean = (double)((double)pixel_sum / (double)i);

        double cache_hit = 0.0f;

        for(i = 0; i < image_size; ++i)
        {
            cache_hit = (double)grayscale_img.image[i][0] - tmp_mean;
            tmp_deviation += cache_hit * cache_hit;
        }

        tmp_deviation *= 1.0f / (double)(image_size - 1);
        tmp_deviation = sqrt(tmp_deviation);

        (*sample_array)[smp_ctr].image = grayscale_img;
        (*sample_array)[smp_ctr].mean_average = tmp_mean;
        (*sample_array)[smp_ctr].standard_deviation = tmp_deviation;

        close_image(&bmp);
    }
}

void fill_std_color_array(pixel_vector* v)
{
    rgb_pixel red = { 0, 0, 255 };
    rgb_pixel green = { 0, 255, 0 };
    rgb_pixel blue = { 255, 0, 0 };
    rgb_pixel yellow = { 0, 255, 255 };
    rgb_pixel cyan = { 255, 255, 0 };
    rgb_pixel gray = { 192, 192, 192 };
    rgb_pixel magenta = { 128, 0, 128 };
    rgb_pixel orange = { 0, 140, 255 };
    rgb_pixel red2 = { 0, 0, 128 };
    rgb_pixel magenta2 = { 255, 0, 255 };

    *v = (pixel_vector)malloc(sizeof(rgb_pixel) * 10);
    pixel_vector vec = *v;

    for(uint32_t i = 0; i < 10; ++i)
    {
        switch(i)
        {
        case 0:
            pixel_eq(vec[i], red);
            break;

        case 1:
            pixel_eq(vec[i], yellow);
            break;

        case 2:
            pixel_eq(vec[i], green);
            break;

        case 3:
            pixel_eq(vec[i], cyan);
            break;

        case 4:
            pixel_eq(vec[i], magenta2);
            break;

        case 5:
            pixel_eq(vec[i], blue);
            break;

        case 6:
            pixel_eq(vec[i], gray);
            break;

        case 7:
            pixel_eq(vec[i], orange);
            break;

        case 8:
            pixel_eq(vec[i], magenta);
            break;

        case 9:
            pixel_eq(vec[i], red2);
            break;

        default:
            break;
        }
    }
}

double test_for_correlation(bmp_image* gs_img, grayscale_image sample, uint32_t y, uint32_t x)
{
    uint32_t sample_height = sample.image.image_height;
    uint32_t sample_width = sample.image.image_width;

    uint32_t gs_height = gs_img->image_height;
    uint32_t gs_width = gs_img->image_width;

    uint32_t max_height = y + sample_height;
    uint32_t max_width = x + sample_width;

    uint32_t sample_total_size = sample_height * sample_width;
    double sample_size = (double)sample_total_size;

    double tmp_mean = 0.0f;
    double std_deviation = 0.0f;
    double correlation_coeff = 0.0f;
    double cache_value = 0.0f;

    for(uint32_t i = y; i < max_height; ++i)
    {
        for(uint32_t j = x; j < max_width; ++j)
        {
            if((i < gs_height) && (j < gs_width))
            {
                tmp_mean += gs_img->image[i * gs_width + j][0];
            }
        }
    }
    tmp_mean = tmp_mean / sample_size;

    if(tmp_mean < 25.0f)
        return -1.0f;

    for(uint32_t i = y; i < max_height; ++i)
    {
        for(uint32_t j = x; j < max_width; ++j)
        {
            if(i >= gs_height || j >= gs_width)
            {
                std_deviation += tmp_mean * tmp_mean;
            }
            else
            {
                cache_value = (double)gs_img->image[i * gs_width + j][0] - tmp_mean;
                std_deviation += cache_value * cache_value;
            }
        }
    }

    std_deviation *= (1.0f / (sample_size - 1.0));
    std_deviation = sqrt(std_deviation);

    double deviation_couple = (1.0f / (std_deviation * sample.standard_deviation));
    double mini_cor = 0.0f;

    for(uint32_t i = y; i < max_height; ++i)
    {
        for(uint32_t j = x; j < max_width; ++j)
        {
            if(i >= gs_height || j >= gs_width)
            {
                mini_cor += (-1.0f * (double)tmp_mean) * ((double)sample.image.image[(i - y) * sample_width + j - x][0] - sample.mean_average);
            }
            else
            {
                mini_cor += ((double)gs_img->image[i * gs_width + j][0] - (double)tmp_mean) * ((double)sample.image.image[(i - y) * sample_width + j - x][0] - sample.mean_average);
            }
        }
    }

    correlation_coeff = deviation_couple * (1.0f / sample_size) * mini_cor;
    return correlation_coeff;
}

correlations perform_matching_operation(bmp_image* gs_img, digit_array sample_array, uint32_t sample_count, double correlation_bias)
{
    correlations tmp = {0, NULL};

    if(sample_count == 0)
        return tmp;

    assert( (gs_img != NULL)  );
    assert( (sample_array != NULL) );

    tmp.correlation_array = malloc(sizeof(correlation_str) * MAX_correlationS);

    uint32_t test_height = gs_img->image_height;
    uint32_t test_width = gs_img->image_width;

    uint32_t sample_height = sample_array[0].image.image_height;
    uint32_t sample_width = sample_array[0].image.image_width;

    uint32_t total_height = test_height + sample_height;
    uint32_t total_width = test_width + sample_width;

    double correlation_result = 0.0f;

    correlation_str tmp_correlation = { 0, 0, 0, 0.0f };

    for(uint32_t i = 0; i < total_height; ++i)
    {
        for(uint32_t j = 0; j < total_width; ++j)
        {
            for(uint32_t k = 0; k < sample_count; ++k)
            {
                if(tmp.correlation_count >= MAX_correlationS)
                    return tmp;

                correlation_result = test_for_correlation(gs_img, sample_array[k], i, j);

                if(correlation_result > correlation_bias)
                {
                    tmp_correlation.sample_used = k;
                    tmp_correlation.x_l = j;
                    tmp_correlation.y_l = i;
                    tmp_correlation.correlation = correlation_result;

                    tmp.correlation_array[tmp.correlation_count] = tmp_correlation;
                    ++ tmp.correlation_count;
                }
            }
        }
    }

    return tmp;
}

void draw_rectangle(bmp_image* bmp, uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2, rgb_pixel col)
{
    if(x1 > bmp->image_width || y1 > bmp->image_height)
        return;

    for(uint32_t i = x1; i <= x2; ++i)
    {
        if(i >= bmp->image_width)
            break;

        pixel_eq(bmp->image[y1 * bmp->image_width + i], col);

        if(y2 >= bmp->image_height)
            continue;
        pixel_eq(bmp->image[y2 * bmp->image_width + i], col);
    }

    for(uint32_t i = y1; i <= y2; ++i)
    {
        if(i >= bmp->image_height)
            break;

        pixel_eq(bmp->image[i * bmp->image_width + x1], col);

        if(x2 >= bmp->image_width)
            continue;
        pixel_eq(bmp->image[i * bmp->image_width + x2], col);
    }
}

double max_helper(uint32_t a, uint32_t b)
{
    if(a > b) return (double)a;
    return (double)b;
}

double min_helper(uint32_t a, uint32_t b)
{
    if(a < b) return (double)a;
    return (double)b;
}

uint8_t is_overlap(uint32_t lx1, uint32_t ly1, uint32_t rx1, uint32_t ry1, uint32_t lx2, uint32_t ly2, uint32_t rx2, uint32_t ry2)
{
    double SA = (double)(rx1 - lx1) * (double)(ry1 - ly1);
    double SB = (double)(rx2 - lx2) * (double)(ry2 - ly2);

    double SI = max_helper(0, min_helper(rx2, rx1) - max_helper(lx2, lx1)) * max_helper(0, min_helper(ry2, ry1) - max_helper(ly2, ly1));

    double SU = SA + SB - SI;
    double ratio = SI / SU;

    if(ratio > 0.2)
        return 1;
    return 0;
}

int compare_cor(const void* lhs, const void* rhs)
{
    correlation_str lh_cor = *(correlation_str*)lhs;
    correlation_str rh_cor = *(correlation_str*)rhs;

    if(lh_cor.correlation < rh_cor.correlation)
        return 1;
    else return -1;
}

correlations reduce_correlations(correlations correlation_list, uint32_t sample_height, uint32_t sample_width)
{
    correlations result = { 0, NULL };

    result.correlation_array = (correlation_str*)malloc(sizeof(correlation_str) * correlation_list.correlation_count);
    qsort(correlation_list.correlation_array, correlation_list.correlation_count, sizeof(correlation_str), compare_cor);

    correlation_str lhs, rhs;

    uint32_t correlations_added = 0;
    uint32_t do_overlap = 0;

    for(uint32_t i = 0; i < correlation_list.correlation_count; ++i)
    {
        lhs = correlation_list.correlation_array[i];
        do_overlap = 0;

        for(uint32_t j = 0; j < i; ++j)
        {
            rhs = correlation_list.correlation_array[j];

            if(is_overlap(lhs.x_l, lhs.y_l, lhs.x_l + sample_width, lhs.y_l + sample_height, rhs.x_l, rhs.y_l, rhs.x_l + sample_width, rhs.y_l + sample_height) == 1)
            {
                do_overlap = 1;
                break;
            }
        }

        if(do_overlap == 0)
        {
            result.correlation_array[correlations_added ++] = correlation_list.correlation_array[i];
            result.correlation_count ++;
        }
    }

    return result;
}

void paint_correlation(bmp_image* bmp, correlations correlation_list, pixel_vector color_array, uint32_t sample_height, uint32_t sample_width)
{
    uint32_t x = 0, y = 0, sample = 0;
    rgb_pixel col_pixel;

    for(uint32_t i = 0; i < correlation_list.correlation_count; ++i)
    {
        x = correlation_list.correlation_array[i].x_l;
        y = correlation_list.correlation_array[i].y_l;
        sample = correlation_list.correlation_array[i].sample_used;
        pixel_eq(col_pixel, color_array[sample]);

        draw_rectangle(bmp, x, x + sample_width, y, y + sample_height, col_pixel);
    }
}

void perform_template_matching(const char* src, const char* sample, const char* extension, const char* new_file_path, uint32_t sample_count)
{
    digit_array digits;
    load_samples(sample, extension, &digits, sample_count);

    bmp_image test_image, grayscale_test;
    read_image(&test_image, src);
    to_grayscale_image(&test_image, &grayscale_test);

    correlations corel = perform_matching_operation(&grayscale_test, digits, sample_count, 0.5f);

    pixel_vector colors = NULL;
    fill_std_color_array(&colors);

    correlations reduced = reduce_correlations(corel, digits[0].image.image_height, digits[0].image.image_width);
    paint_correlation(&test_image, reduced, colors, digits[0].image.image_height, digits[0].image.image_width);

    write_image(&test_image, new_file_path);

    for(uint32_t i = 0; i < sample_count; ++i)
        free(digits[i].image.image);

    free(digits);
    free(colors);
    free(corel.correlation_array);
    free(reduced.correlation_array);
    free(grayscale_test.image);
}
