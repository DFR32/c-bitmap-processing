#ifndef UTILS_H
#define UTILS_H
#include <stdint.h>

typedef uint8_t rgb_pixel[3];
typedef uint8_t bmp_header[54];
typedef rgb_pixel* pixel_vector;

void swap_pixels(rgb_pixel a, rgb_pixel b);
void pixel_eq(rgb_pixel a, rgb_pixel b);
void xor_pixel_with_value(rgb_pixel tmp, uint32_t value);
void xor_pixel_with_pixel(rgb_pixel lhs, rgb_pixel rhs);
#endif // UTILS_H
