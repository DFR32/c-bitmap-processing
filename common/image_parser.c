#include "image_parser.h"
#include <string.h>

void read_image(bmp_image* img, const char* path)
{
    uint64_t image_read_result = 0;
    uint32_t image_pixel_size = 0;

    int32_t image_size;

    // Obtain a handle to the file
    file_ptr image_file_ptr = fopen(path, "rb");

    fseek(image_file_ptr, 0, SEEK_END);
    image_size = ftell(image_file_ptr);
    fseek(image_file_ptr, 0, SEEK_SET);

    assert( (image_size > sizeof(bmp_header)) );

    // Assert in case the pointer is null
    assert(image_file_ptr != NULL);

    image_read_result = fread(&img->header, 1, sizeof(img->header), image_file_ptr);
    assert( (image_read_result == sizeof(img->header)) );

    // Assign the values read from the array
    img->image_width = *(uint32_t*)((uint8_t*)&img->header + 18);
    img->image_height = *(uint32_t*)((uint8_t*)&img->header + 22);

    // Set the padding accordingly
    if(img->image_width % 4 != 0)
        img->padding = 4 - (3 * img->image_width) % 4;
    else
        img->padding = 0;

    assert( (image_size == (img->image_width  * img->image_height * sizeof(rgb_pixel) + sizeof(bmp_header) + img->padding * img->image_height) ) );

    // Calculate the size of the pixel matrix in bytes
    image_pixel_size = 3 * img->image_height * img->image_width;

    // Read the pixel data
    img->image = (pixel_vector)(malloc(image_pixel_size));

    assert( (img->image != NULL) );

    for(int32_t i = img->image_height - 1; i >= 0; --i)
    {
        fread(img->image + i * img->image_width, 3 * img->image_width, 1, image_file_ptr);

        if(img->padding != 0)
            fseek(image_file_ptr, img->padding, SEEK_CUR);
    }

    // Close the handle
    fclose(image_file_ptr);
}

void write_image(bmp_image* img, const char* path)
{
    // Open the handle
    file_ptr image_file_ptr = fopen(path, "wb");

    // Verify its integrity
    assert( (image_file_ptr != NULL) );

    // Write the header
    fwrite(img->header, 1, sizeof(img->header), image_file_ptr);

    uint8_t* null_padding_array = (uint8_t*)(calloc(img->padding, 3));

    assert( (null_padding_array != NULL) );

    // Write the image matrix
    for(int32_t i = img->image_height - 1; i >= 0; --i)
    {
        fwrite(img->image + i * img->image_width, 3 * img->image_width, 1, image_file_ptr);

        if(img->padding != 0)
            fwrite(null_padding_array, img->padding, 1, image_file_ptr);
    }

    // Free alloc'd resources
    free(null_padding_array);

    // Close the handle
    fclose(image_file_ptr);
    free(img->image);
}

void close_image(bmp_image* img)
{
    free(img->image);
    img->image = 0;
}
