#ifndef IMAGE_PARSER_H
#define IMAGE_PARSER_H
#include "utils.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef FILE* file_ptr;

typedef struct
{
    bmp_header header;
    uint32_t image_width;
    uint32_t image_height;
    uint32_t padding;
    pixel_vector image;
} bmp_image;

void read_image (bmp_image* img, const char* path);
void write_image(bmp_image* img, const char* path);
void close_image(bmp_image* img);
#endif // IMAGE_PARSER_H
