#include "utils.h"

void swap_pixels(rgb_pixel a, rgb_pixel b)
{
    uint8_t tmp = 0;

    for(int8_t i = 0; i < 3; ++i)
    {
        tmp = a[i];
        a[i] = b[i];
        b[i] = tmp;
    }
}

void pixel_eq(rgb_pixel a, rgb_pixel b)
{
    a[0] = b[0];
    a[1] = b[1];
    a[2] = b[2];
}

void xor_pixel_with_value(rgb_pixel tmp, uint32_t value)
{
    tmp[2] ^= (value >> 16) & 0xFF;
    tmp[1] ^= (value >> 8) & 0xFF;
    tmp[0] ^= value & 0xFF;
}

void xor_pixel_with_pixel(rgb_pixel lhs, rgb_pixel rhs)
{
    lhs[0] ^= rhs[0];
    lhs[1] ^= rhs[1];
    lhs[2] ^= rhs[2];
}
