#ifndef MAIN_IMAGE_PROCESSING
#include "common/utils.h"
#include "common/image_parser.h"
#include "pattern_helper.h"
#include "string.h"

int main_image_processing(int argc, char** argv);
#endif // MAIN_IMAGE_PROCESSING
