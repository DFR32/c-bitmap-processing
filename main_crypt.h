#ifndef MAIN_CRYPT
#define MAIN_CRYPT

#define MAX_PATH            128

#include "common/image_parser.h"
#include "crypto.h"

int main_crypt(int argc, char** argv);
#endif // MAIN_CRYPT
