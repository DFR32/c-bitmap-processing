#include "main_image_processing.h"

int main_image_processing(int argc, char** argv)
{
    char* file_path   = (char*)calloc(MAX_PATH + 1, sizeof(char));
    char* sample_path = (char*)calloc(MAX_PATH + 1, sizeof(char));
    char* sample_ext  = (char*)calloc(MAX_PATH + 1, sizeof(char));
    char* n_file_path = (char*)calloc(MAX_PATH + 1, sizeof(char));
    uint32_t sample_count = 0;

    assert( (file_path != NULL) );
    assert( (sample_path != NULL) );
    assert( (sample_ext != NULL) );

    printf("Please write the name (with the extension) of the file to perform template matching on:\n");
    scanf("%128s", file_path);

    printf("Please write the path to the samples (using the sample's name up until the digit is met)\n");
    scanf("%128s", sample_path);

    printf("Please write the extension of the sample (WITHOUT THE DOT!)\n");
    scanf("%128s", sample_ext);

    printf("Please write the destination path (where to save the matched file)\n");
    scanf("%128s", n_file_path);

    printf("Please write the number of samples to use\n");
    scanf("%u", &sample_count);

    perform_template_matching(file_path, sample_path, sample_ext, n_file_path, sample_count);

    free(file_path);
    free(sample_path);
    free(sample_ext);
    free(n_file_path);
    return 0;
}
