#ifndef CRYPTO_H
#define CRYPTO_H
#include "common/image_parser.h"

typedef uint32_t* random_array;
typedef uint32_t* permutation_vector;
typedef double* chi_array;

void encrypt_image(const char* seed_file, bmp_image* image_ptr);
void decrypt_image(const char* seed_file, bmp_image* image_ptr);
void chi_result(bmp_image* image_ptr, chi_array chi);
void reset_chi_result(chi_array chi);
#endif // CRYPTO_H
