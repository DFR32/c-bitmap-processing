#ifndef PATTERN_HELPER
#define PATTERN_HEPLER
#include "common/utils.h"
#include "common/image_parser.h"

#define MAX_PATH            128
#define MAX_correlationS     10000

typedef struct
{
    bmp_image image;
    double mean_average; // 0.0 - 255.0
    double standard_deviation;
} grayscale_image;

typedef struct
{
    uint32_t sample_used;
    uint32_t x_l;
    uint32_t y_l;
    double correlation;
} correlation_str;

typedef struct
{
    uint32_t correlation_count;
    correlation_str* correlation_array;
} correlations;

typedef grayscale_image* digit_array;

void perform_template_matching(const char* src, const char* sample, const char* extension, const char* new_file_path, uint32_t sample_count);
#endif // PATTERN_HELPER
