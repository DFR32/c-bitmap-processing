#include "crypto.h"
#include "common/utils.h"

#include <string.h>
#include <math.h>

uint32_t xorshift32(uint32_t x)
{
	/* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;

	return x;
} // from Wikipedia

void chi_result(bmp_image* image_ptr, chi_array chi)
{
    double estimated_frequency = (double)(image_ptr->image_width * image_ptr->image_height) / 256.00f;

    double* R_channel = (double*)(calloc(768, sizeof(double)));
    double* G_channel = (double*)((uint8_t*)R_channel + 256 * sizeof(double));
    double* B_channel = (double*)((uint8_t*)G_channel + 256 * sizeof(double));

    for(int32_t i = 0; i < (image_ptr->image_width * image_ptr->image_height); ++i)
    {
        ++ R_channel[image_ptr->image[i][2]];
        ++ G_channel[image_ptr->image[i][1]];
        ++ B_channel[image_ptr->image[i][0]];
    }

    for(int32_t i = 0; i < 256; ++i)
    {
        chi[0] += (R_channel[i] - estimated_frequency) * (R_channel[i] - estimated_frequency) / estimated_frequency;
        chi[1] += (G_channel[i] - estimated_frequency) * (G_channel[i] - estimated_frequency) / estimated_frequency;
        chi[2] += (B_channel[i] - estimated_frequency) * (B_channel[i] - estimated_frequency) / estimated_frequency;
    }

    free(R_channel);
}

void reset_chi_result(chi_array chi)
{
    chi[0] = 0.0f;
    chi[1] = 0.0f;
    chi[2] = 0.0f;
}

void fill_random_array(random_array* to_fill, uint32_t height, uint32_t width, uint32_t initial_seed)
{
    uint32_t array_size = 2 * height * width;

    *to_fill = (random_array)(malloc(sizeof(uint32_t) * array_size));

    assert( (to_fill != NULL) );

    (*to_fill)[0] = initial_seed;

    for(uint32_t i = 0; i < array_size - 1; ++i)
    {
        (*to_fill)[i + 1] = xorshift32((*to_fill)[i]);
    }
}

void generate_permutation_vector(random_array rand_array, permutation_vector perm_vector, int32_t max_size)
{
    for(int32_t i = 0; i < max_size; ++i) perm_vector[i] = i;

    int32_t j = 0;

    uint32_t tmp = 0;

    for(int32_t i = max_size - 1; i > 0; --i)
    {
        j = rand_array[max_size - i] % (i + 1);

        tmp = perm_vector[i];
        perm_vector[i] = perm_vector[j];
        perm_vector[j] = tmp;
    }
}

void apply_permutation(bmp_image* img, permutation_vector perm_vector)
{
    int32_t limit_size = img->image_height * img->image_width;

    pixel_vector new_image = (pixel_vector)(malloc(3 * limit_size));

    for(int32_t i = 0; i < limit_size; ++i)
    {
        pixel_eq(new_image[perm_vector[i]], img->image[i]);
    }

    pixel_vector old_vector = img->image;
    img->image = new_image;
    free(old_vector);
}

void apply_inverse_permutation(bmp_image* img, permutation_vector perm_vector)
{
    int32_t limit_size = img->image_height * img->image_width;

    pixel_vector new_image = (pixel_vector)(malloc(3 * limit_size));

    for(int32_t i = 0; i < limit_size; ++i)
    {
        pixel_eq(new_image[i], img->image[perm_vector[i]]);
    }

    pixel_vector old_vector = img->image;
    img->image = new_image;
    free(old_vector);
}

void permute_image(random_array rand_array, bmp_image* bmp)
{
    int32_t random_vector_size = bmp->image_height * bmp->image_width;

    permutation_vector perm_vector = (permutation_vector)(malloc(sizeof(uint32_t) * random_vector_size));

    assert( (perm_vector != NULL) );

    generate_permutation_vector(rand_array, perm_vector, random_vector_size);
    apply_permutation(bmp, perm_vector);
    free(perm_vector);
}

void inverse_permute_image(random_array rand_array, bmp_image* bmp)
{
    int32_t random_vector_size = bmp->image_height * bmp->image_width;

    permutation_vector perm_vector = (permutation_vector)(malloc(sizeof(uint32_t) * random_vector_size));

    assert( (perm_vector != NULL) );

    generate_permutation_vector(rand_array, perm_vector, random_vector_size);
    apply_inverse_permutation(bmp, perm_vector);
    free(perm_vector);
}

void encrypt_image(const char* seed_file, bmp_image* image_ptr)
{
    random_array fill_array = NULL;
    uint32_t seed_value = 0, random_value = 0;

    file_ptr seed_file_ptr = fopen(seed_file, "r");

    assert( (seed_file_ptr != NULL) );

    int32_t file_scanf_result = fscanf(seed_file_ptr, "%u %u", &seed_value, &random_value);

    assert( (file_scanf_result == 2) );

    fill_random_array(&fill_array, image_ptr->image_height, image_ptr->image_width, seed_value);

    permute_image(fill_array, image_ptr);

    uint32_t full_image_size = image_ptr->image_height * image_ptr->image_width;

    for(uint32_t i = 0; i < full_image_size; ++i)
    {
        if(i == 0)
        {
            xor_pixel_with_value(image_ptr->image[i], random_value);
            xor_pixel_with_value(image_ptr->image[i], fill_array[full_image_size]);
        }
        else
        {
            xor_pixel_with_pixel(image_ptr->image[i], image_ptr->image[i - 1]);
            xor_pixel_with_value(image_ptr->image[i], fill_array[i + full_image_size]);
        }
    }

    free(fill_array);
    fclose(seed_file_ptr);
}

void decrypt_image(const char* seed_file, bmp_image* image_ptr)
{
    random_array fill_array = NULL;
    uint32_t seed_value = 0, random_value = 0;

    file_ptr seed_file_ptr = fopen(seed_file, "r");

    assert( (seed_file_ptr != NULL) );

    int32_t file_scanf_result = fscanf(seed_file_ptr, "%u %u", &seed_value, &random_value);

    assert( (file_scanf_result == 2) );

    fill_random_array(&fill_array, image_ptr->image_height, image_ptr->image_width, seed_value);

    // Calculate the image size
    uint32_t full_image_size = image_ptr->image_height * image_ptr->image_width;

    // Duplicate the array in order to decrypt
    pixel_vector dup_image = (pixel_vector)malloc(3 * sizeof(uint8_t) * full_image_size);
    memcpy(dup_image, image_ptr->image, 3 * sizeof(uint8_t) * full_image_size);

    for(uint32_t i = 0; i < full_image_size; ++i)
    {
        if(i == 0)
        {
            xor_pixel_with_value(image_ptr->image[i], random_value);
            xor_pixel_with_value(image_ptr->image[i], fill_array[full_image_size]);
        }
        else
        {
            xor_pixel_with_value(image_ptr->image[i], fill_array[i + full_image_size]);
            xor_pixel_with_pixel(image_ptr->image[i], dup_image[i - 1]);
        }
    }

    // Free the duplicate array
    free(dup_image);

    // Calculate inverse permutation
    inverse_permute_image(fill_array, image_ptr);

    // Free the random number array
    free(fill_array);

    fclose(seed_file_ptr);
}
